#!/usr/bin/env python
import urllib2
import os
import subprocess

URL = "http://alpha.wallhaven.cc/search?categories=110&purity=100&sorting=random&order=asc"
userAgent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0'


#DBUS_SESSION_BUS_ADDRESS is now required in order to set a wallpaper
def set_envir():
    pid_find = subprocess.Popen(
        ["pgrep", "gnome-session"],
        stdout=subprocess.PIPE).communicate()[0].\
        decode("utf-8").replace("\n", "")
    cmd = "grep -z DBUS_SESSION_BUS_ADDRESS /proc/"+\
          pid_find+"/environ|cut -d= -f2-"
    os.environ["DBUS_SESSION_BUS_ADDRESS"] = subprocess.Popen(
        ['/bin/bash', '-c', cmd],
        stdout=subprocess.PIPE).communicate()[0].\
        decode("utf-8").replace("\n","").replace("\0", "")

#parse random page for first result
request = urllib2.Request(URL)
request.add_header('Referer','http://alpha.wallhaven.cc/')
request.add_header('User-Agent',userAgent)
response = urllib2.urlopen(request)
pageSource = response.read()
addressStart = pageSource.find("<a class=\"preview\" href=\"") + 25
addressEnd = pageSource.find("\"", addressStart)
specificImagePage = pageSource[addressStart:addressEnd]

#parse first result for full high res image URL
request = urllib2.Request(specificImagePage)
request.add_header('Referer','http://alpha.wallhaven.cc/')
request.add_header('User-Agent',userAgent)
response = urllib2.urlopen(request)
pageSource = response.read()
addressStart = pageSource.find("         src=\"") + 14
addressEnd = pageSource.find("\"", addressStart)
fullWallpaperURL = "http:" + pageSource[addressStart:addressEnd]

#get pic name
slashPos = fullWallpaperURL.rfind("/") + 1
picName = fullWallpaperURL[slashPos:]

#download full res image
bashCommand = "wget " + fullWallpaperURL
os.system(bashCommand)

#delete previous background image
p = subprocess.Popen("gsettings get org.gnome.desktop.background picture-uri", shell=True, stdout=subprocess.PIPE)
oldPicAddress = p.stdout.read()
oldPicStart = oldPicAddress.rfind("/") + 1
oldPicEnd = oldPicAddress.rfind("'")
oldPic = oldPicAddress[oldPicStart:oldPicEnd]
os.system("rm -f " + os.getcwd() + "/" + oldPic)

#set background to new image
set_envir()
bashCommand = "gsettings set org.gnome.desktop.background picture-uri file://" + os.getcwd() + "/" + picName
os.system(bashCommand)
