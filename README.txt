Requirements
Python 2.7.3 (un-tested with other versions)

Tested on Ubuntu 14.04 LTS x64

To use:
#1. Create a folder in your home directory called autowallpaperchanger
#2. Put changer.py into the autowallpaperchanger folder
#3. Open a console and type "crontab -e"
#4. Paste the following line at the bottom of the file:

*/15 * * * * cd ~/autowallpaperchanger/; DISPLAY=:0.0 ~/autowallpaperchanger/changer.py > /tmp/changer.log

By default, the cron script does the following:
#1. Executes every 15 minutes, on the quarter hour
#2. Moves into the autowallpaper directory located in the users home folder
#3. DISPLAY=:0.0 tells the script to execute on the current users active monitor (it's required, or else the wallpaper doesn't change)
#4. Executes the changer.py script
#5. Shits out a log in the tmp directory
